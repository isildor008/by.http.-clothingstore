package by.http.clothingstore.command;

import by.http.clothingstore.model.entuty.User;
import by.http.clothingstore.service.UserService;
import by.http.clothingstore.service.impl.UserServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * Created by Dmitry on 30.06.2017.
 */
public class SuccessSignupPage implements CommandAction {

    private UserService userService;

    public SuccessSignupPage(){
        userService = new UserServiceImpl();
    }

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String firstName = request.getParameter("firstName");
        String lastName= request.getParameter("lastName");
        String email= request.getParameter("firstEmail");
        String password= request.getParameter("firstPassword");
        String page;

        User user;
        try {

            user = userService.signup(firstName, lastName, email, password);
            page = "/WEB-INF/page/successSipnupPage.jsp";

        } catch (Exception e) {
            page = "/WEB-INF/page/404.jsp";
        }

        return page;
    }
}
