package by.http.clothingstore.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Created by Dmitry on 28.06.2017.
 */
public class LogoutAction implements CommandAction {
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        HttpSession session=request.getSession();
        session.invalidate();
        return "/WEB-INF/page/indexPage.jsp";
    }
}
