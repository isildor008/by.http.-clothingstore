package by.http.clothingstore.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by Dmitry on 09.06.2017.
 */
public class LoginPage implements CommandAction {
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        return "/WEB-INF/page/loginPage.jsp";
    }
}
