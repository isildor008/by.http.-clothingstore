
package by.http.clothingstore.command;


import by.http.clothingstore.command.commandAdmin.commadUser.*;
import by.http.clothingstore.command.commandAdmin.commandCategory.*;
import by.http.clothingstore.command.commandAdmin.commandItemParam.*;

import static by.http.clothingstore.util.ConstantValue.ACTION_AUTHORISE;

/**
 * Created by Dmitry on 05.06.2017.
 */
public class CommandChoser {
    public static CommandAction chooseAction(String action) {
        if (ACTION_AUTHORISE.equals(action)) {

            System.out.println("Login action " + action);
            return new LoginCommandAction();
        }

        if ("logPage".equals(action)) {

            System.out.println("logPage " + action);
            return new LoginPage();
        }
        if ("start".equals(action)) {

            System.out.println("start " + action);
            return new HomePage();
        }
        if ("login".equals(action)) {

            System.out.println("login " + action);
            return new LoginCommandAction();
        }
        if ("card".equals(action)) {

            System.out.println("card " + action);
            return new CardAction();
        }
        if ("contact".equals(action)) {

            System.out.println("contact " + action);
            return new ContactAction();
        }
        if ("shop".equals(action)) {

            System.out.println("shop " + action);
            return new ShopAction();
        }
        if ("logout".equals(action)) {

            System.out.println("logout " + action);
            return new LogoutAction();
        }

        if ("signup".equals(action)) {
            System.out.println("signup " + action);
            return new SuccessSignupPage();
        }

        if ("editUser".equals(action)) {
            System.out.println("editUser" + action);
            return new EditUserAction();
        }


        if ("saveEditUser".equals(action)) {
            System.out.println("saveEditUser " + action);
            return new SaveEditUser();
        }

        if ("removeUser".equals(action)) {
            System.out.println("removeUser " + action);
            return new RemoveUser();
        }

        if ("viewCategory".equals(action)) {
            System.out.println("viewCategory " + action);
            return new ViewCategory();
        }
        if ("editCategory".equals(action)) {
            System.out.println("editCategory " + action);
            return new EditCategoryAction();
        }
        if ("saveEditCategory".equals(action)) {
            System.out.println("saveEditCategory " + action);
            return new SaveEditCategory();
        }
        if ("deleteCategory".equals(action)) {
            System.out.println("deleteCategory " + action);
            return new DeleteCategory();
        }
        if ("createCategoryAction".equals(action)) {
            System.out.println("createCategoryAction " + action);
            return new CreateCategoryAction();
        }
        if ("saveCreateCategory".equals(action)) {
            System.out.println("saveCreateCategory " + action);
            return new SaveCreateCategory();
        }

        if ("createItemParamAction".equals(action)) {
            System.out.println("createItemParamAction " + action);
            return new CreateItemParamAction();
        }
        if ("deleteItemParam".equals(action)) {
            System.out.println("deleteItemParam " + action);
            return new DeleteItemParam();
        }

        if ("editItemParamAction".equals(action)) {
            System.out.println("editItemParamAction " + action);
            return new EditItemParamAction();
        }
        if ("saveCreateItemParam".equals(action)) {
            System.out.println("saveCreateItemParam " + action);
            return new SaveCreateItemParam();
        }
        if ("saveEditItemParam".equals(action)) {
            System.out.println("saveEditItemParam " + action);
            return new SaveEditItemParam();
        }

        if ("viewItemParam".equals(action)) {
            System.out.println("viewItemParam " + action);
            return new ViewItemParam();
        }
        return null;

    }
}
