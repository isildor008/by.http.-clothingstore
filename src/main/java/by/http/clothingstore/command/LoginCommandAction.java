package by.http.clothingstore.command;


import by.http.clothingstore.model.entuty.User;
import by.http.clothingstore.service.UserService;
import by.http.clothingstore.service.impl.UserServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;


/**
 * Created by Dmitry on 05.06.2017.
 */
public class LoginCommandAction implements CommandAction {

    private UserService userService;


    public LoginCommandAction() {
        userService = new UserServiceImpl();

    }

    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String login = request.getParameter("email");
        String password = request.getParameter("password");
        String page=null;

        User user;
        try {

            user = userService.authotise(login, password);

            if (user.getRolle() == 0) {
                HttpSession session = request.getSession();
                session.setAttribute("userName", user.getEmail());
                request.setAttribute("userName", user.getEmail());
                List<User> list = userService.list();
                request.setAttribute("user", list);
              //  response.sendRedirect("AdminServlet?action=getAllUser");
                request.getRequestDispatcher("/WEB-INF/page/admin/allUserPage.jsp").forward(request, response);


            } else {

                System.out.println(user.getEmail());
                HttpSession session = request.getSession();
                session.setAttribute("userName", user.getEmail());
                request.setAttribute("userName", user.getEmail());
                page = "/WEB-INF/page/indexPage.jsp";
            }


        } catch (Exception e) {
            page = "/WEB-INF/page/404.jsp";
        }
        return page;
    }
}
