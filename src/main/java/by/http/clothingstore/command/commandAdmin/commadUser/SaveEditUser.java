package by.http.clothingstore.command.commandAdmin.commadUser;

import by.http.clothingstore.command.CommandAction;
import by.http.clothingstore.model.entuty.User;
import by.http.clothingstore.service.UserService;
import by.http.clothingstore.service.impl.UserServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Created by Dmitry on 06.07.2017.
 */
public class SaveEditUser implements CommandAction {

    private UserService userService;

    public SaveEditUser() {
        userService = new UserServiceImpl();
    }

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {


        Integer userId = Integer.valueOf(request.getParameter("userId"));
        String email = request.getParameter("email");
        String firstName = request.getParameter("firstName");
        String lastName = request.getParameter("lastName");
        Double balance = Double.valueOf(request.getParameter("balance"));
        Integer rolle = Integer.valueOf(request.getParameter("rolle"));
        Integer status = Integer.valueOf(request.getParameter("status"));
        List<User> list = userService.list();
        request.setAttribute("user", list);
        String page;



        try {
            userService.edit(userId, firstName, lastName, email, rolle, balance, status);
            page = "/WEB-INF/page/admin/allUserPage.jsp";



        } catch (Exception e) {
            page = "/WEB-INF/page/404.jsp";
        }


        return page;
    }
}
