package by.http.clothingstore.command.commandAdmin.commadUser;

import by.http.clothingstore.command.CommandAction;
import by.http.clothingstore.model.entuty.User;
import by.http.clothingstore.service.UserService;
import by.http.clothingstore.service.impl.UserServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Created by Dmitry on 07.07.2017.
 */
public class RemoveUser implements CommandAction {

    private UserService userService;

    public RemoveUser(){
        userService = new UserServiceImpl();
    }

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        Integer userId = Integer.valueOf(request.getParameter("userId"));
        String page;
        try {
            userService.remove(userId);
            List<User> list = userService.list();
            request.setAttribute("user", list);
            page = "/WEB-INF/page/admin/allUserPage.jsp";

        } catch (Exception e) {
            page = "/WEB-INF/page/404.jsp";
        }

        return page;
    }


}
