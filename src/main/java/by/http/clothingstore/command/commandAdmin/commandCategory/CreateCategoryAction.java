package by.http.clothingstore.command.commandAdmin.commandCategory;

import by.http.clothingstore.command.CommandAction;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by Dmitry on 13.07.2017.
 */
public class CreateCategoryAction implements CommandAction {
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {

        return "/WEB-INF/page/admin/createCategoryPage.jsp";
    }
}
