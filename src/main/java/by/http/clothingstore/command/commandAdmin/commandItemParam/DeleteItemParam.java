package by.http.clothingstore.command.commandAdmin.commandItemParam;

import by.http.clothingstore.command.CommandAction;
import by.http.clothingstore.model.entuty.ItemParam;
import by.http.clothingstore.service.ItemParamService;
import by.http.clothingstore.service.impl.ItemParamServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Created by Dmitry on 14.07.2017.
 */
public class DeleteItemParam implements CommandAction {

    private ItemParamService itemParamService;

    public DeleteItemParam(){
        itemParamService=new ItemParamServiceImpl();
    }

    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String page;
        Integer id= Integer.valueOf(request.getParameter("itemParamId"));
        try {
            itemParamService.delete(id);
            List<ItemParam> list =itemParamService.getAllItemParam();
            request.setAttribute("itemParam", list);
            page = "WEB-INF/page/admin/allItemParamPage.jsp";

        } catch (Exception e) {
            page = "/WEB-INF/page/404.jsp";
        }

        return page;
    }
}
