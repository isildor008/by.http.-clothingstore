package by.http.clothingstore.command.commandAdmin.commandItemParam;

import by.http.clothingstore.command.CommandAction;
import by.http.clothingstore.service.ItemParamService;
import by.http.clothingstore.service.impl.ItemParamServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by Dmitry on 14.07.2017.
 */
public class CreateItemParamAction implements CommandAction {

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {

        return "/WEB-INF/page/admin/createItemParamPage.jsp";
    }
}
