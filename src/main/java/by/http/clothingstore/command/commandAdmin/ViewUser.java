package by.http.clothingstore.command.commandAdmin;

import by.http.clothingstore.command.CommandAction;
import by.http.clothingstore.model.entuty.User;
import by.http.clothingstore.service.UserService;
import by.http.clothingstore.service.impl.UserServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Created by Dmitry on 10.07.2017.
 */
public class ViewUser implements CommandAction {

    private UserService userService;

    public ViewUser(){
        userService = new UserServiceImpl();
    }
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        List<User> list = userService.list();
        request.setAttribute("user", list);

        return "/WEB-INF/page/admin/allUserPage.jsp";
    }
}
