package by.http.clothingstore.command.commandAdmin.commandCategory;

import by.http.clothingstore.command.CommandAction;
import by.http.clothingstore.model.entuty.Category;
import by.http.clothingstore.service.CategoryService;
import by.http.clothingstore.service.impl.CategoryServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Created by Dmitry on 13.07.2017.
 */
public class DeleteCategory implements CommandAction {

    private CategoryService categoryService;

    public DeleteCategory(){
        categoryService=new CategoryServiceImpl();
    }
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        Integer categoryId = Integer.valueOf(request.getParameter("categoryId"));
        String page;
        try {
            categoryService.delete(categoryId);
            List<Category> list = categoryService.getAllCategory();
            request.setAttribute("category", list);
            page = "/WEB-INF/page/admin/allCategoryPage.jsp";

        } catch (Exception e) {
            page = "/WEB-INF/page/404.jsp";
        }

        return page;
    }

}
