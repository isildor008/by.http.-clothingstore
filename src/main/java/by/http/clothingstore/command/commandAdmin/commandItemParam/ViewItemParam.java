package by.http.clothingstore.command.commandAdmin.commandItemParam;

import by.http.clothingstore.command.CommandAction;
import by.http.clothingstore.model.entuty.ItemParam;
import by.http.clothingstore.service.ItemParamService;
import by.http.clothingstore.service.impl.ItemParamServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Created by Dmitry on 14.07.2017.
 */
public class ViewItemParam implements CommandAction {

    private ItemParamService itemParamService;

    public ViewItemParam(){
        itemParamService=new ItemParamServiceImpl();
    }

    @Override
        public String execute(HttpServletRequest request, HttpServletResponse response) {

        String page=null;

        try {
            List<ItemParam> list =itemParamService.getAllItemParam();
            request.setAttribute("itemParam", list);

            page = "WEB-INF/page/admin/allItemParamPage.jsp";
        } catch (Exception e) {
            page = "/WEB-INF/page/404.jsp";
        }


        return page;
    }
}
