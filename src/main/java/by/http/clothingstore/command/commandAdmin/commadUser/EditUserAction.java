package by.http.clothingstore.command.commandAdmin.commadUser;

import by.http.clothingstore.command.CommandAction;
import by.http.clothingstore.model.entuty.User;
import by.http.clothingstore.service.UserService;
import by.http.clothingstore.service.impl.UserServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by Dmitry on 03.07.2017.
 */
public class EditUserAction implements CommandAction {

    private UserService userService;

    public EditUserAction(){
        userService = new UserServiceImpl();
    }

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String userId = request.getParameter("userId");

        User user;
        try {
             user=userService.getById(Integer.valueOf(userId));
            request.setAttribute("userId", user.getUserId());
            request.setAttribute("email", user.getEmail());
            request.setAttribute("firstName", user.getFirstName());
            request.setAttribute("lastName", user.getLastName());
            request.setAttribute("balance", user.getBalance());
            request.setAttribute("rolle", user.getRolle());
            request.setAttribute("status", user.getUserStatus());


            }
            catch (Exception e){e.printStackTrace();}



        return "/WEB-INF/page/editUserPage.jsp";
    }
}
