package by.http.clothingstore.command.commandAdmin.commandItemParam;

import by.http.clothingstore.command.CommandAction;
import by.http.clothingstore.model.entuty.ItemParam;
import by.http.clothingstore.service.ItemParamService;
import by.http.clothingstore.service.impl.ItemParamServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by Dmitry on 14.07.2017.
 */
public class EditItemParamAction implements CommandAction {

    private ItemParamService itemParamService;

    public EditItemParamAction(){
        itemParamService=new ItemParamServiceImpl();
    }

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String itemParamId = request.getParameter("itemParamId");
        ItemParam itemParam;
        try {
            itemParam=itemParamService.getById(Integer.valueOf(itemParamId));
            request.setAttribute("itemParamId", itemParam.getItemParamId());
            request.setAttribute("itemParamTitle", itemParam.getItemParamTitle());
            request.setAttribute("itemParamPriority", itemParam.getItemParamPriority());

        } catch (Exception e) {
            e.printStackTrace();
        }

        return "WEB-INF/page/admin/editItemParamPage.jsp";
    }
}
