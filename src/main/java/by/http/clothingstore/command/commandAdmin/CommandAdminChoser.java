package by.http.clothingstore.command.commandAdmin;

import by.http.clothingstore.command.CommandAction;

/**
 * Created by Dmitry on 10.07.2017.
 */
public class CommandAdminChoser {
    public static CommandAction chooseAdminAction(String action) {
        if ("getAllUser".equals(action)) {

            System.out.println("Login action " + action);
            return new ViewUser();
        }
        return null;
    }
}
