package by.http.clothingstore.command.commandAdmin.commandCategory;

import by.http.clothingstore.command.CommandAction;
import by.http.clothingstore.model.entuty.Category;
import by.http.clothingstore.service.CategoryService;
import by.http.clothingstore.service.impl.CategoryServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Created by Dmitry on 13.07.2017.
 */
public class SaveEditCategory implements CommandAction {
    private CategoryService categoryService;

    public SaveEditCategory(){
        categoryService=new CategoryServiceImpl();
    }

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String page;
        Integer id=Integer.valueOf(request.getParameter("categoryId"));
        String title= request.getParameter("categoryTitle");
        Integer parent=Integer.valueOf(request.getParameter("categoryParent"));
        Integer priority=Integer.valueOf(request.getParameter("categoryPriority"));
        try {
            categoryService.update(id, title,parent,priority);
            List<Category> list = categoryService.getAllCategory();
            request.setAttribute("category", list);
            page = "/WEB-INF/page/admin/allCategoryPage.jsp";
        } catch (Exception e) {
            page = "/WEB-INF/page/404.jsp";
        }


        return page;
    }
}
