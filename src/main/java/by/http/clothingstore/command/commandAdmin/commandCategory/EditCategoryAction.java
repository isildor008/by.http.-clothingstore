package by.http.clothingstore.command.commandAdmin.commandCategory;

import by.http.clothingstore.command.CommandAction;
import by.http.clothingstore.model.entuty.Category;
import by.http.clothingstore.service.CategoryService;
import by.http.clothingstore.service.impl.CategoryServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by Dmitry on 13.07.2017.
 */
public class EditCategoryAction implements CommandAction {

    private CategoryService categoryService;

    public EditCategoryAction() {
        categoryService = new CategoryServiceImpl();
    }

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String categoryId = request.getParameter("categoryId");
        Category category;
        try {
            category = categoryService.getById(Integer.valueOf(categoryId));
            request.setAttribute("categoryId", category.getCategoryId());
            request.setAttribute("categoryTitle", category.getCategoryTitle());
            request.setAttribute("categoryParent", category.getCategoryParent());
            request.setAttribute("categoryPriority", category.getCategoryPriority());

        } catch (Exception e) {
            e.printStackTrace();
        }


        return "/WEB-INF/page/admin/editCategoryPage.jsp";
    }
}
