package by.http.clothingstore.command.commandAdmin.commandCategory;

import by.http.clothingstore.command.CommandAction;
import by.http.clothingstore.model.entuty.Category;
import by.http.clothingstore.service.CategoryService;
import by.http.clothingstore.service.impl.CategoryServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by Dmitry on 10.07.2017.
 */
public class ViewCategory implements CommandAction {

    private CategoryService categoryService;

    public ViewCategory(){
        categoryService=new CategoryServiceImpl();
    }

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String page=null;


        try {
            List<Category> list=categoryService.getAllCategory();

            request.setAttribute("category", list);

            page = "WEB-INF/page/admin/allCategoryPage.jsp";
        } catch (Exception e) {
            page = "/WEB-INF/page/404.jsp";
        }


        return page;
    }
}
