package by.http.clothingstore.command;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



/**
 * Created by Dmitry on 05.06.2017.
 */
public class HomePage implements CommandAction  {


    public String execute(HttpServletRequest request, HttpServletResponse response) {


        return "/WEB-INF/page/indexPage.jsp";
    }

}
