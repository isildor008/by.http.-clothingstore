package by.http.clothingstore.dao;



import by.http.clothingstore.model.entuty.User;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by Dmitry on 04.06.2017.
 */
public interface UserDao  {

    User fetchById (String email, String password);
    List<User> getUserList();
    User save(String firstName, String lastName, String email, String password);
    User edit(Integer userId, String firstName, String lastName, String email, Integer rolle, Double balance, Integer status);
    User getById(Integer id);
    void remove(Integer id);

}