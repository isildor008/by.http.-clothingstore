package by.http.clothingstore.dao;

import by.http.clothingstore.model.entuty.Category;

import java.util.List;

/**
 * Created by Dmitry on 07.07.2017.
 */
public interface CategoryDao {

    void save (String title, Integer parent, Integer priority);
    void delete(Integer id);
    void update(Integer id, String title, Integer parent, Integer priority);
    List<Category> getAllCategory();
    Category getById(Integer id);

}
