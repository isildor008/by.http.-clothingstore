package by.http.clothingstore.dao.impl;

import by.http.clothingstore.dao.ItemParamDao;
import by.http.clothingstore.model.entuty.ItemParam;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Created by Dmitry on 13.07.2017.
 */
public class ItemParamDaoImpl implements ItemParamDao {

    ItemParam itemParam = new ItemParam();
    ResourceBundle rb = ResourceBundle.getBundle("config", Locale.getDefault());
    String dbUrl = rb.getString("db.url");
    String dbLog = rb.getString("db.login");
    String dbPass = rb.getString("db.pass");
    String dbDriver = rb.getString("db.driver");

    @Override
    public void save(String title, Integer priority) {

        try {
            try {
                Class.forName(dbDriver);
                Connection conn = DriverManager.getConnection(dbUrl, dbLog, dbPass);
                PreparedStatement ps = conn.prepareStatement("INSERT INTO `shop`.`item_param` (`item_param_title`, `item_param_priority`) VALUES (?, ?)");
                ps.setString(1, title);
                ps.setInt(2, priority);
                ps.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void delete(Integer id) {
        try {
            try {
                Class.forName(dbDriver);
                Connection conn = DriverManager.getConnection(dbUrl, dbLog, dbPass);
                PreparedStatement ps = conn.prepareStatement("DELETE FROM `shop`.`item_param` WHERE  `item_param_id`=?");
                ps.setInt(1, id);
                ps.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }


    }

    @Override
    public void update(Integer id, String title, Integer priority) {
        try {
            try {
                Class.forName(dbDriver);
                Connection conn = DriverManager.getConnection(dbUrl, dbLog, dbPass);
                PreparedStatement ps = conn.prepareStatement("UPDATE  `shop`.`item_param`  SET `item_param_title`=?,`item_param_priority`=? WHERE `item_param_id`=?");
                ps.setString(1, title);
                ps.setInt(2, priority);
                ps.setInt(3, id);
                ps.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

    }

    @Override
    public List<ItemParam> getAllItemParam() {
        List<ItemParam> list = new ArrayList<>();
        try {
            try {
                Class.forName(dbDriver);
                Connection conn = DriverManager.getConnection(dbUrl, dbLog, dbPass);
                PreparedStatement ps = conn.prepareStatement("select `item_param_id`, `item_param_title`, `item_param_priority` from item_param");
                ResultSet rs = ps.executeQuery();
                while (rs.next()) {
                    ItemParam itemParam = new ItemParam(rs.getInt(1), rs.getString(2), rs.getInt(3));
                    list.add(itemParam);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return list;
    }

    @Override
    public ItemParam getById(Integer id) {

        try {
            try {
                Class.forName(dbDriver);
                Connection conn = DriverManager.getConnection(dbUrl, dbLog, dbPass);
                PreparedStatement ps = conn.prepareStatement("select `item_param_id`, `item_param_title`, `item_param_priority`from item_param where `item_param_id`=?");
                ps.setInt(1, id);

                ResultSet rs = ps.executeQuery();
                while (rs.next()) {
                    itemParam.setItemParamId(rs.getInt(1));
                    itemParam.setItemParamTitle(rs.getString(2));
                    itemParam.setItemParamPriority(rs.getInt(3));
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        return itemParam;
    }
}
