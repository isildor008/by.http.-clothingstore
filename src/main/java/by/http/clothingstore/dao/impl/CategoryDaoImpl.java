package by.http.clothingstore.dao.impl;

import by.http.clothingstore.dao.CategoryDao;
import by.http.clothingstore.model.entuty.Category;
import by.http.clothingstore.util.DataBaseUtil;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Created by Dmitry on 07.07.2017.
 */
public class CategoryDaoImpl implements CategoryDao {

    Category category = new Category();

    ResourceBundle rb = ResourceBundle.getBundle("config", Locale.getDefault());
    String dbUrl = rb.getString("db.url");
    String dbLog = rb.getString("db.login");
    String dbPass = rb.getString("db.pass");
    String dbDriver = rb.getString("db.driver");

    @Override
    public void save(String title, Integer parent, Integer priority) {

        try {
            try {
                Class.forName(dbDriver);
                Connection conn = DriverManager.getConnection(dbUrl, dbLog, dbPass);
                PreparedStatement ps = conn.prepareStatement("INSERT INTO `shop`.`category` (`category_title`, `category_parent`, `priority`) VALUES (?, ?, ?)");
                ps.setString(1, title);
                ps.setInt(2, parent);
                ps.setInt(3, priority);
                ps.executeUpdate();

            } catch (SQLException e) {
        e.printStackTrace();
    }
} catch (ClassNotFoundException e) {
        e.printStackTrace();
        }
    }

    @Override
    public void delete(Integer id) {
        Connection connection =null;

            try {
                if(connection==null||connection.isClosed()){
                    connection = DataBaseUtil.getConnection();
                }
//                Class.forName(dbDriver);
//                Connection conn = DriverManager.getConnection(dbUrl, dbLog, dbPass);
                PreparedStatement ps = connection.prepareStatement(" DELETE FROM `shop`.`category` WHERE  `category_id`=?");
                ps.setInt(1, id);
                ps.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
            }



    }

    @Override
    public void update(Integer id, String title, Integer parent, Integer priority) {

        try {
            try {
                Class.forName(dbDriver);
                Connection conn = DriverManager.getConnection(dbUrl, dbLog, dbPass);
                PreparedStatement ps = conn.prepareStatement("UPDATE `shop`.`category` SET `category_title`=?,`category_parent`=?,`priority`=? WHERE  `category_id`=?;");
                ps.setString(1, title);
                ps.setInt(2, parent);
                ps.setInt(3, priority);
                ps.setInt(4, id);
                ps.executeUpdate();

            } catch (SQLException e) {
                e.printStackTrace();
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Category> getAllCategory() {
        List<Category> list=new ArrayList<>() ;

        try {
            try {
                Class.forName(dbDriver);
                Connection conn = DriverManager.getConnection(dbUrl, dbLog, dbPass);
                PreparedStatement ps = conn.prepareStatement("select `category_id`, `category_title`, `category_parent`, `priority` from category");
                ResultSet rs = ps.executeQuery();
                while (rs.next()) {
              Category category=new Category(rs.getInt(1),rs.getString(2),rs.getInt(3),rs.getInt(4));
                    list.add(category);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        return list;
    }

    @Override
    public Category getById(Integer id) {
        try {
            try {
                Class.forName(dbDriver);
                Connection conn = DriverManager.getConnection(dbUrl, dbLog, dbPass);
                PreparedStatement ps = conn.prepareStatement("select `category_id`, `category_title`, `category_parent`, `priority` from category where `category_id`=?");
                ps.setInt(1, id);

                ResultSet rs = ps.executeQuery();
                while (rs.next()) {
                    category.setCategoryId(rs.getInt(1));
                    category.setCategoryTitle(rs.getString(2));
                    category.setCategoryParent(rs.getInt(3));
                    category.setCategoryPriority(rs.getInt(4));
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        return category;
    }
}
