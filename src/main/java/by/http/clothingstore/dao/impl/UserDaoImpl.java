package by.http.clothingstore.dao.impl;


import by.http.clothingstore.dao.UserDao;
import by.http.clothingstore.model.entuty.User;

import java.math.BigDecimal;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;


/**
 * Created by Dmitry on 05.06.2017.
 */
public class UserDaoImpl implements UserDao {

    User user = new User();

    ResourceBundle rb = ResourceBundle.getBundle("config", Locale.getDefault());
    String dbUrl = rb.getString("db.url");
    String dbLog = rb.getString("db.login");
    String dbPass = rb.getString("db.pass");
    String dbDriver = rb.getString("db.driver");


    public User fetchById(String email, String password) {
        try {
            try {
                Class.forName(dbDriver);
                Connection conn = DriverManager.getConnection(dbUrl, dbLog, dbPass);
                PreparedStatement ps = conn.prepareStatement("select * from user where email= ? AND password=?");
                ps.setString(1, email);
                ps.setString(2, password);
                ResultSet rs = ps.executeQuery();

                if (rs.next()) {
                    String log = rs.getString(4);
                    String pass = rs.getString(5);
                    Integer rolle = rs.getInt(6);
                    user.setEmail(log);
                    user.setPassword(pass);
                    user.setRolle(rolle);
                }


            } catch (SQLException e) {
                e.printStackTrace();
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return user;
    }

    public List<User> getUserList() {
        List<User> list = new ArrayList<>();
        try {
            try {
                Class.forName(dbDriver);
                Connection conn = DriverManager.getConnection(dbUrl, dbLog, dbPass);
                PreparedStatement ps = conn.prepareStatement("select `user_id`, `first_name`, `last_name`, `email`, `rolle_id`, `user_balance`, `status` from user where `rolle_id`=1");
                ResultSet rs = ps.executeQuery();
                while (rs.next()) {
                    Integer userId = rs.getInt(1);
                    String firstName = rs.getString(2);
                    String lastName = rs.getString(3);
                    String email = rs.getString(4);
                    Integer rolle = rs.getInt(5);
                    BigDecimal balance = rs.getBigDecimal(6);
                    Integer userStatus = rs.getInt(7);
                    User user = new User(userId, firstName, lastName, email, rolle, balance, userStatus);
                    list.add(user);

                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return list;
    }


    public User save(String firstName, String lastName, String email, String password) {
        try {
            try {
                Class.forName(dbDriver);
                Connection conn = DriverManager.getConnection(dbUrl, dbLog, dbPass);
                PreparedStatement ps = conn.prepareStatement("INSERT INTO `shop`.`user` (`first_name`, `last_name`, `email`, `password`, `rolle_id`, `user_balance`, `status`) VALUES (?, ?, ?, ?, ?, ?, ?)");
                ps.setString(1, firstName);
                ps.setString(2, lastName);
                ps.setString(3, email);
                ps.setString(4, password);
                ps.setInt(5, 0);
                ps.setDouble(6, 0);
                ps.setInt(7, 0);
                ps.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return user;
    }


    public User edit(Integer userId, String firstName, String lastName, String email, Integer rolle, Double balance, Integer status) {
        try {
            try {
                Class.forName(dbDriver);
                Connection conn = DriverManager.getConnection(dbUrl, dbLog, dbPass);
                PreparedStatement ps = conn.prepareStatement("UPDATE `shop`.`user` SET `first_name`=?,`last_name`=?,`email`=?,`rolle_id`=?,`user_balance`=?,`status`=? WHERE  `user_id`=?;");
                ps.setString(1, firstName);
                ps.setString(2, lastName);
                ps.setString(3, email);
                ps.setInt(4, rolle);
                ps.setDouble(5, balance);
                ps.setInt(6, status);
                ps.setInt(7, userId);
                ps.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return user;
    }

    @Override
    public User getById(Integer id) {
        try {
            try {
                Class.forName(dbDriver);
                Connection conn = DriverManager.getConnection(dbUrl, dbLog, dbPass);
                PreparedStatement ps = conn.prepareStatement("select `user_id`, `first_name`, `last_name`, `email`, `rolle_id`, `user_balance`, `status` from user where `user_id`=?");
                ps.setInt(1, id);

                ResultSet rs = ps.executeQuery();
                while (rs.next()) {
                    Integer userId = rs.getInt(1);
                    String firstName = rs.getString(2);
                    String lastName = rs.getString(3);
                    String email = rs.getString(4);
                    Integer rolle = rs.getInt(5);
                    BigDecimal balance = rs.getBigDecimal(6);
                    Integer userStatus = rs.getInt(7);
                    user.setUserId(userId);
                    user.setFirstName(firstName);
                    user.setLastName(lastName);
                    user.setEmail(email);
                    user.setRolle(rolle);
                    user.setBalance(balance);
                    user.setUserStatus(userStatus);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return user;
    }




    public void remove(Integer id) {
        try {
            try {
                Class.forName(dbDriver);
                Connection conn = DriverManager.getConnection(dbUrl, dbLog, dbPass);
                PreparedStatement ps = conn.prepareStatement("  DELETE FROM `shop`.`user` WHERE  `user_id`=?");
                ps.setInt(1, id);
                ps.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

    }
}
