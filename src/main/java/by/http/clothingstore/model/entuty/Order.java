package by.http.clothingstore.model.entuty;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Created by Dmitry on 04.06.2017.
 */
public class Order implements Serializable {

    private Integer orderId;
    private Integer userId;
    private Timestamp orderTime;

}
