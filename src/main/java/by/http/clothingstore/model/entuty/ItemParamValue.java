package by.http.clothingstore.model.entuty;

import java.io.Serializable;

/**
 * Created by Dmitry on 04.06.2017.
 */
public class ItemParamValue implements Serializable {

    private Integer itemParamValueId;
    private Integer itemParamId;
    private Integer itemId;
    private String itemParamValue;

}
