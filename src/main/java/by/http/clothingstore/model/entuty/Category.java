package by.http.clothingstore.model.entuty;

import java.io.Serializable;

/**
 * Created by Dmitry on 04.06.2017.
 */
public class Category implements Serializable{

    private Integer categoryId;
    private String categoryTitle;
    private Integer categoryParent;
    private Integer categoryPriority;

    public Category(){}

    public Category(Integer categoryId, String categoryTitle, Integer categoryParent, Integer categoryPriority) {
        this.categoryId = categoryId;
        this.categoryTitle = categoryTitle;
        this.categoryParent = categoryParent;
        this.categoryPriority = categoryPriority;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryTitle() {
        return categoryTitle;
    }

    public void setCategoryTitle(String categoryTitle) {
        this.categoryTitle = categoryTitle;
    }

    public Integer getCategoryParent() {
        return categoryParent;
    }

    public void setCategoryParent(Integer categoryParent) {
        this.categoryParent = categoryParent;
    }

    public Integer getCategoryPriority() {
        return categoryPriority;
    }

    public void setCategoryPriority(Integer categoryPriority) {
        this.categoryPriority = categoryPriority;
    }

    @Override
    public String toString() {
        return "Category{" +
                "categoryId=" + categoryId +
                ", categoryTitle='" + categoryTitle + '\'' +
                ", categoryParent=" + categoryParent +
                ", categoryPriority=" + categoryPriority +
                '}';
    }
}
