package by.http.clothingstore.model.entuty;

import java.io.Serializable;

/**
 * Created by Dmitry on 04.06.2017.
 */
public class ItemParam implements Serializable {

    private Integer itemParamId;
    private String itemParamTitle;
    private Integer itemParamPriority;

    public ItemParam(){

    }

    public ItemParam(Integer itemParamId, String itemParamTitle, Integer itemParamPriority) {
        this.itemParamId = itemParamId;
        this.itemParamTitle = itemParamTitle;
        this.itemParamPriority = itemParamPriority;
    }

    public Integer getItemParamId() {
        return itemParamId;
    }

    public void setItemParamId(Integer itemParamId) {
        this.itemParamId = itemParamId;
    }

    public String getItemParamTitle() {
        return itemParamTitle;
    }

    public void setItemParamTitle(String itemParamTitle) {
        this.itemParamTitle = itemParamTitle;
    }

    public Integer getItemParamPriority() {
        return itemParamPriority;
    }

    public void setItemParamPriority(Integer itemParamPriority) {
        this.itemParamPriority = itemParamPriority;
    }
}
