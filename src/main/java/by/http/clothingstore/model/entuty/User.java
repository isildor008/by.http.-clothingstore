package by.http.clothingstore.model.entuty;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Created by Dmitry on 04.06.2017.
 */
public class User implements Serializable {

    private Integer userId;
    private String firstName;
    private String lastName;
    private String email;
    private String password;
    private Integer rolle;
    private BigDecimal balance;
    private Integer userStatus;

    public  User(){}

    public User(Integer userId, String firstName, String lastName, String email, Integer rolle, BigDecimal balance, Integer userStatus) {
        this.userId = userId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.rolle = rolle;
        this.balance = balance;
        this.userStatus = userStatus;
    }



    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getRolle() {
        return rolle;
    }

    public void setRolle(Integer rolle) {
        this.rolle = rolle;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public Integer getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(Integer userStatus) {
        this.userStatus = userStatus;
    }

    @Override
    public String toString() {
        return "User{" +
                "userId=" + userId +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", rolle=" + rolle +
                ", balance=" + balance +
                ", userStatus=" + userStatus +
                '}';
    }
}
