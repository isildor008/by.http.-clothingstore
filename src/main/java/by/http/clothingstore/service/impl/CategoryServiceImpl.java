package by.http.clothingstore.service.impl;

import by.http.clothingstore.dao.CategoryDao;
import by.http.clothingstore.dao.impl.CategoryDaoImpl;
import by.http.clothingstore.model.entuty.Category;
import by.http.clothingstore.service.CategoryService;

import java.util.List;

/**
 * Created by Dmitry on 10.07.2017.
 */
public class CategoryServiceImpl implements CategoryService {


    {
        dao = new CategoryDaoImpl();
    }

    private CategoryDao dao;

    @Override
    public void save(String title, Integer parent, Integer priority) {

        dao.save(title, parent, priority);

    }

    @Override
    public void delete(Integer id) {
        dao.delete(id);
    }

    @Override
    public void update(Integer id, String title, Integer parent, Integer priority) {
        dao.update(id, title, parent, priority);
    }

    @Override
    public List<Category> getAllCategory() {

        return dao.getAllCategory();
    }

    @Override
    public Category getById(Integer id) {

        return dao.getById(id);
    }
}
