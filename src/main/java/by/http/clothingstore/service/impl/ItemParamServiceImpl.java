package by.http.clothingstore.service.impl;

import by.http.clothingstore.dao.ItemParamDao;
import by.http.clothingstore.dao.impl.ItemParamDaoImpl;
import by.http.clothingstore.model.entuty.ItemParam;
import by.http.clothingstore.service.ItemParamService;

import java.util.List;

/**
 * Created by Dmitry on 13.07.2017.
 */
public class ItemParamServiceImpl implements ItemParamService{

    {
        dao=new ItemParamDaoImpl();
    }
    private ItemParamDao dao;

    @Override
    public void save(String title, Integer priority) {
        dao.save(title,priority);

    }

    @Override
    public void delete(Integer id) {
        dao.delete(id);
    }

    @Override
    public void update(Integer id, String title, Integer priority) {
        dao.update(id,title,priority);

    }

    @Override
    public List<ItemParam> getAllItemParam() {
        return dao.getAllItemParam();
    }

    @Override
    public ItemParam getById(Integer id) {
        return dao.getById(id);
    }
}
