package by.http.clothingstore.service.impl;


import by.http.clothingstore.dao.UserDao;
import by.http.clothingstore.dao.impl.UserDaoImpl;
import by.http.clothingstore.model.entuty.User;
import by.http.clothingstore.service.UserService;

import java.util.List;

/**
 * Created by Dmitry on 05.06.2017.
 */
public class UserServiceImpl implements UserService {

    {
        dao = new UserDaoImpl();
    }

    private UserDao dao;

    public User authotise(String email, String password) {
        User user = dao.fetchById(email, password);

        return user;
    }

    public boolean logOut(User user) {
        return false;
    }

    public List<User> list() {
        return dao.getUserList();
    }

    @Override
    public User signup(String firstName, String lastName, String email, String password) {

        User user=dao.save(firstName, lastName, email, password);
        return user;
    }

    @Override
    public User edit(Integer userId, String firstName, String lastName, String email, Integer rolle, Double balance, Integer status) {
        User user=dao.edit(userId,firstName,lastName,email,rolle,balance,status);
        return user;
    }

    public User getById(Integer id){
        User user=dao.getById(id);
        return user;
    }

    @Override
    public void remove(Integer id) {
        dao.remove(id);
    }

}
