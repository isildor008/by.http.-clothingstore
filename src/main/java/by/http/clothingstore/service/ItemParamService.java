package by.http.clothingstore.service;

import by.http.clothingstore.model.entuty.ItemParam;

import java.util.List;

/**
 * Created by Dmitry on 13.07.2017.
 */
public interface ItemParamService  {
    void save (String title, Integer priority);
    void delete(Integer id);
    void update(Integer id, String title,  Integer priority);
    List<ItemParam> getAllItemParam();
    ItemParam getById(Integer id);
}
