package by.http.clothingstore.service;


import by.http.clothingstore.model.entuty.User;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by Dmitry on 05.06.2017.
 */
public interface UserService {
    User authotise(String login, String password);

    boolean logOut(User user);

    List<User> list();

    User signup(String firstName, String lastName, String email, String password);

    User edit(Integer userId, String firstName, String lastName, String email, Integer rolle, Double balance, Integer status);

    User getById(Integer id);

    void remove(Integer id);
}
