package by.http.clothingstore.util;

/**
 * Created by Dmitry on 04.06.2017.
 */
public class ConstantValue {

    private ConstantValue(){}

    public static final String ACTION_AUTHORISE = "login";
    public static final String REQUEST_PARAM_LOGIN = "login";
    public static final String REQUEST_PARAM_PASSWORD = "pass";
    public static final String PAGE_ERROR = "/error.jsp";
    public static final String REQUEST_PARAM_LIST_EQ = "list_eq";
    public static final String PAGE_ADMIN_MAIN = "/admin.jsp";
    public static final String PAGE_USER_MAIN = "/user.jsp";
    public static final Integer ADMIN = 0;
    public static final Integer USER = 1;

}
