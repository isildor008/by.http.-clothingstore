<%--
  Created by IntelliJ IDEA.
  User: Dmitry
  Date: 13.07.2017
  Time: 14:01
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<section id="form"><!--form-->
    <div class="container">
        <div class="row">
            <div class="col-sm-4 col-sm-offset-1">
                <div class="login-form">
                    <h2>Edit your Account</h2>
                    <form  action="MainServlet" method="post">
                        <input id="categoryTitle" type="text" autofocus placeholder="categoryTitle" name="categoryTitle" />

                        <input  type="text"   placeholder="Parent"  name="categoryParent"/>
                        <input  type="text"   placeholder="Priority"  name="categoryPriority"/>
                        <input type="hidden" value="saveCreateCategory" name="action"/>
                        <button type="submit " class="btn btn-default">Submit</button>

                    </form>
                </div>
            </div>
        </div>
    </div>

</section>