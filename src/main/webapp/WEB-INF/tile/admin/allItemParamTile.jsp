<%--
  Created by IntelliJ IDEA.
  User: Dmitry
  Date: 14.07.2017
  Time: 13:51
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<section>
    <div class="container">
        <div class="row">
            <div class="panel-group category-products">
                <div class="table-responsive">
                    <a href="/MainServlet?action=createItemParamAction" class="action-icon"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Item parametr id</th>
                            <th>Title</th>
                            <th>Priority</th>

                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${itemParam}" var="itemParam">
                            <tr>
                                <td><c:out value="${itemParam.itemParamId}"/></td>
                                <td><c:out value="${itemParam.itemParamTitle}"/></td>
                                <td><c:out value="${itemParam.itemParamPriority}"/></td>

                                <td><a href="/MainServlet?action=deleteItemParam&itemParamId=${itemParam.itemParamId}" class="action-icon"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                    <a href="/MainServlet?action=editItemParamAction&itemParamId=${itemParam.itemParamId}" class="action-icon"><i class="fa fa-pencil" aria-hidden="true"></i></a>

                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>
</section>

