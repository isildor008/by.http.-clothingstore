<%--
  Created by IntelliJ IDEA.
  User: Dmitry
  Date: 30.06.2017
  Time: 16:13
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<section>
    <div class="container">
        <div class="row">
            <div class="panel-group category-products">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>UserId</th>
                            <th>FirstName</th>
                            <th>LastName</th>
                            <th>Email</th>
                            <th>Role</th>
                            <th>Balance</th>
                            <th>UserStatus</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${user}" var="userList">
                            <tr>
                                <td><c:out value="${userList.getUserId()}"/></td>
                                <td><c:out value="${userList.getFirstName()}"/></td>
                                <td><c:out value="${userList.getLastName()}"/></td>
                                <td><c:out value="${userList.getEmail()}"/></td>
                                <td><c:out value="${userList.getRolle()}"/></td>
                                <td><c:out value="${userList.getBalance()}"/></td>
                                <td><c:out value="${userList.getUserStatus()}"/></td>
                                <td><a href="/MainServlet?action=removeUser&userId=${userList.getUserId()}" class="action-icon"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                    <a href="/MainServlet?action=editUser&userId=${userList.getUserId()}" class="action-icon"><i class="fa fa-pencil" aria-hidden="true"></i></a>

                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>
</section>

