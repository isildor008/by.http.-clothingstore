<%--
  Created by IntelliJ IDEA.
  User: Dmitry
  Date: 10.07.2017
  Time: 13:45
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<section>
    <div class="container">
        <div class="row">
            <div class="panel-group category-products">
                <div class="table-responsive">
                    <a href="/MainServlet?action=createCategoryAction" class="action-icon"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Category id</th>
                            <th>Title</th>
                            <th>Parent id</th>
                            <th>Priority</th>

                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${category}" var="category">
                            <tr>
                                <td><c:out value="${category.categoryId}"/></td>
                                <td><c:out value="${category.categoryTitle}"/></td>
                                <td><c:out value="${category.categoryParent}"/></td>
                                <td><c:out value="${category.categoryPriority}"/></td>
                                <td><a href="/MainServlet?action=deleteCategory&categoryId=${category.categoryId}" class="action-icon"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                    <a href="/MainServlet?action=editCategory&categoryId=${category.categoryId}" class="action-icon"><i class="fa fa-pencil" aria-hidden="true"></i></a>

                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>
</section>
