<%--
  Created by IntelliJ IDEA.
  User: Dmitry
  Date: 14.07.2017
  Time: 13:56
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<section id="form"><!--form-->
    <div class="container">
        <div class="row">
            <div class="col-sm-4 col-sm-offset-1">
                <div class="login-form">
                    <h2>Edit your Account</h2>
                    <form  action="MainServlet" method="post">
                        <input id="itemParamTitle" type="text" autofocus placeholder="itemParamTitle" name="itemParamTitle" />
                        <input  type="text"   placeholder="Priority"  name="itemParamPriority"/>
                        <input type="hidden" value="saveCreateItemParam" name="action"/>
                        <button type="submit " class="btn btn-default">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

</section>