<%--
  Created by IntelliJ IDEA.
  User: Dmitry
  Date: 07.07.2017
  Time: 11:59
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<header id="header"><!--header-->


    <div class="header_top"><!--header_top-->
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="contactinfo">
                        <ul class="nav nav-pills">
                            <li><a href="#"><i class="fa fa-phone"></i> +375 29 891 99 62</a></li>
                            <li><a href="#"><i class="fa fa-envelope"></i> info@domain.com</a></li>
                        </ul>
                    </div>
                </div>

            </div>
        </div>
    </div><!--/header_top-->


    <div class="header-middle"><!--header-middle-->
        <div class="container">
            <div class="row">
                <div class="col-sm-4">
                    <div class="logo pull-left">
                        <a href="MainServlet?action=start"><img src="resources/images/home/logo.png" alt=""></a>
                    </div>
                    <div class="btn-group pull-right">
                        <div class="btn-group">
                            <button type="button" class="btn btn-default dropdown-toggle usa" data-toggle="dropdown">
                                English
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu">
                                <li><a href="#">English</a></li>
                                <li><a href="#">Русский</a></li>
                            </ul>
                        </div>

                        <div class="btn-group">
                            <button type="button" class="btn btn-default dropdown-toggle usa" data-toggle="dropdown">
                                DOLLAR
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu">
                                <li><a href="#">Dollar</a></li>
                                <li><a href="#">Рубль</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-sm-8">
                    <div class="shop-menu pull-right">
                        <ul class="nav navbar-nav">
                            <c:choose>
                                <c:when test="${userName!=null}">
                                    <li><a href="MainServlet?action=login"><i class="fa fa-user"></i> <c:out value="${userName}"/></li>
                                    <li><a href="MainServlet?action=logout"><i class="fa fa-lock"></i> Users</a></li>
                                    <li><a href="MainServlet?action=viewCategory"><i class="fa fa-lock"></i> Category</a></li>
                                    <li><a href="MainServlet?action=logout"><i class="fa fa-lock"></i> Items</a></li>
                                    <li><a href="MainServlet?action=viewItemParam"><i class="fa fa-lock"></i> Item Parametr</a></li>
                                    <li><a href="MainServlet?action=logout"><i class="fa fa-lock"></i> Orders</a></li>
                                    <li><a href="MainServlet?action=logout"><i class="fa fa-lock"></i> Logout</a></li>

                                </c:when>
                                <c:otherwise>
                                    <li><a href="#"><i class="fa fa-user"></i> Account</a></li>
                                    <li><a href="MainServlet?action=logPage"><i class="fa fa-lock"></i> Login</a></li>
                                </c:otherwise>
                            </c:choose>


                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div><!--/header-middle-->


    <div class="header-bottom"><!--header-bottom-->
        <div class="container">
            <div class="row">
                <div class="col-sm-9">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse"
                                data-target=".navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <div class="mainmenu pull-left">
                        <ul class="nav navbar-nav collapse navbar-collapse">
                            <li><a href="MainServlet?action=start" class="active">Home</a></li>
                            <li class="dropdown"><a href="MainServlet?action=shop">Shop<i class="fa fa-angle-down"></i></a>
                                <ul role="menu" class="sub-menu">
                                    <li><a href="MainServlet?action=shop"></i>Products </a></li>
                                    <li><a href="product-details.html">Product Details</a></li>
                                    <li><a href="checkout.html">Checkout</a></li>
                                    <li><a href="cart.html">Cart</a></li>

                                </ul>
                            </li>


                            <li><a href="MainServlet?action=contact">Contact</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="search_box pull-right">
                        <input type="text" placeholder="Search">
                    </div>
                </div>
            </div>
        </div>
    </div><!--/header-bottom-->

</header>