<%--
  Created by IntelliJ IDEA.
  User: Dmitry
  Date: 03.07.2017
  Time: 14:23
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<section id="form"><!--form-->
    <div class="container">
        <div class="row">
            <div class="col-sm-4 col-sm-offset-1">
                <div class="login-form">
                    <h2>Edit your Account</h2>
                    <form  action="MainServlet" method="post">
                        <input id="email" type="text" autofocus placeholder="email" name="email" value="${email}"/>
                        <input type="hidden" value="${userId}" name="userId"/>
                        <input  type="text"   placeholder="Last Name"  name="firstName" value="${firstName}"/>
                        <input  type="text"   placeholder="Last Name"  name="lastName" value="${lastName}"/>
                        <input  type="text"   placeholder="Balance"  name="balance" value="${balance}"/>
                        <input  type="text"   placeholder="Rolle"  name="rolle" value="${rolle}"/>
                        <input  type="text"   placeholder="Status"  name="status" value="${status}"/>
                        <input type="hidden" value="saveEditUser" name="action"/>
                        <button type="submit " class="btn btn-default">Submit</button>

                    </form>
                </div>
            </div>
        </div>
    </div>

</section>